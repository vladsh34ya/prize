<?php

namespace common\models;


use common\services\BankApi;

class BankHelper
{
    /**
     * @param AccountMovement $transaction
     *
     * @return bool
     */
    public static function sendToBank($transaction)
    {
        return BankApi::sendTransaction($transaction->user->account_no, $transaction->id, abs($transaction->amount));
    }
}