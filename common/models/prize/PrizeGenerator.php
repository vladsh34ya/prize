<?php

namespace common\models\prize;

class PrizeGenerator
{
    public static function generateNew()
    {
        //Получим случайный приз
        /** @var Prize $className */
        $prizeType      = self::getRandomType(Prize::getPrizeTypes());
        $generateParams = self::getGenerateParamsByType($prizeType);
        $className      = self::getClassName($prizeType);

        $prize = $className::generate($generateParams);

        if (!$prize) {
            //Если случайный приз не вернулся, значит лимитные призы закончились. Выдадим безлимитный (бонусы)
            $prizeType      = self::getRandomType([Prize::TYPE_BONUS]);
            $generateParams = self::getGenerateParamsByType($prizeType);
            $className      = self::getClassName($prizeType);

            $prize = $className::generate($generateParams);
        }

        return $prize;
    }


    private static function getRandomType($types)
    {
        $randomIndex = array_rand($types, 1);

        return $types[ $randomIndex ];
    }


    private static function getClassName($type)
    {
        return 'common\models\prize\\' . $type;
    }


    private static function getGenerateParamsByType($type)
    {
        if ($type == Prize::TYPE_MONEY) {
            return ['amount' => \Yii::$app->params['maxAmountForMoneyPrize']];
        } elseif ($type == Prize::TYPE_BONUS) {
            return ['amount' => \Yii::$app->params['maxAmountForBonusPrize']];
        }

        return null;
    }
}