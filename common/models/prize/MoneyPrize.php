<?php

namespace common\models\prize;

use common\models\prize\fond\MoneyFond;
use common\models\User;

class MoneyPrize extends Prize
{
    const STATUS_NEW                  = 1;

    const STATUS_TO_ACCOUNT           = 2;

    const STATUS_TO_BONUS             = 3;

    const DEFAULT_CONVERT_COEFFICIENT = 10;

    public $type = 'MoneyPrize';

    public $name = 'Денежный приз';

    public $amount;

    public $transactionId;


    public function getFields()
    {
        return ['name', 'type', 'status', 'amount', 'transactionId'];
    }


    public static function generate($params = null)
    {
        if (!$params || !isset($params['amount'])) {
            return null;
        }

        $availableAmount = MoneyFond::getAvailableAmount();
        $maxAmount       = ($availableAmount > $params['amount']) ? $params['amount'] : $availableAmount;

        $prize         = new static();
        $prize->amount = rand(1, $maxAmount); //TODO 1?
        $prize->status = self::STATUS_NEW;

        return ($prize->amount > 0) ? $prize : null;
    }


    public function accept()
    {
        MoneyFond::decrease($this->amount);
    }


    public function getPrizeAmount()
    {
        return $this->amount . ' руб.';
    }


    public function getIsCompleted()
    {
        return ($this->status == self::STATUS_NEW) ? false : true;
    }


    public function getStatusName()
    {
        $statusNames = [
            self::STATUS_NEW        => 'Новый',
            self::STATUS_TO_ACCOUNT => 'Перечислено на банковский счет',
            self::STATUS_TO_BONUS   => 'Конвертировано в бонусные баллы',
        ];

        return $statusNames[ $this->status ];
    }


    public function getOperations()
    {
        if ($this->status == self::STATUS_NEW) {
            return [
                'sendToAccount'  => 'Вывести на банковский счет',
                'convertToBonus' => 'Конвертировать в бонусные баллы',
            ];
        }

        return null;
    }


    public function executeOperation($name, $user)
    {
        //Проверим, что операция доступна
        $availableOperations = $this->getOperations();

        if (!$availableOperations || !array_key_exists($name, $availableOperations)) {
            return;
        }

        $this->$name($user);
    }


    /**
     * Создаем транзакцию для переведо средств на банк.счет пользователя
     *
     * @param User $user
     */
    private function sendToAccount($user)
    {
        $this->status = self::STATUS_TO_ACCOUNT;

        $this->transactionId = $user->addWithdrawals($this->amount);
    }


    /**
     * Конвертируем деньги в бонусы
     *
     * @param User $user
     */
    private function convertToBonus($user)
    {
        $this->status = self::STATUS_TO_BONUS;

        $coeff = isset(\Yii::$app->params['convertToBonusCoefficient'])
            ? \Yii::$app->params['convertToBonusCoefficient'] :
            self::DEFAULT_CONVERT_COEFFICIENT;

        $this->transactionId = $user->addBonus($this->amount * $coeff);
    }

}