<?php

namespace common\models\prize;

use common\models\prize\fond\ThingFond;

class ThingPrize extends Prize
{
    const STATUS_NEW        = 1;

    const STATUS_IN_PROCESS = 2;

    const STATUS_SEND       = 3;

    const STATUS_RECEIVE    = 4;

    public $type = 'ThingPrize';

    public $thingId;


    public function getFields()
    {
        return ['name', 'type', 'status', 'thingId'];
    }


    public static function generate($params = null)
    {
        $things = ThingFond::getThings();
        if (!$things || count($things) == 0) {
            return null;
        }

        $randomIndex = array_rand($things, 1);
        $thing       = $things[ $randomIndex ];

        $prize          = new static();
        $prize->name    = $thing->name;
        $prize->thingId = $thing->id;
        $prize->status  = self::STATUS_NEW;

        return $prize;
    }


    public function accept()
    {
        ThingFond::decrease($this->thingId);
    }


    public function getPrizeAmount()
    {
        return '1 шт';
    }


    public function getIsCompleted()
    {
        return ($this->status == self::STATUS_RECEIVE) ? true : false;
    }


    public function getOperations()
    {
        if ($this->status == self::STATUS_NEW) {
            return [
                'orderDelivery' => 'Заказать доставку',
            ];
        } elseif ($this->status == self::STATUS_SEND) {
            return [
                'complete' => 'Приз получен',
            ];
        }

        return null;
    }


    public function getAdminOperations()
    {
        if ($this->status == self::STATUS_IN_PROCESS) {
            return [
                'post' => 'Отправить клиенту',
            ];
        }

        return null;
    }


    public function getStatusName()
    {
        $statusNames = [
            self::STATUS_NEW        => 'Новый',
            self::STATUS_IN_PROCESS => 'В обработке',
            self::STATUS_SEND       => 'Отправлен почтовой службой',
            self::STATUS_RECEIVE    => 'Получен',
        ];

        return $statusNames[ $this->status ];
    }


    public function executeOperation($name, $user)
    {
        //Проверим, что операция доступна
        $availableOperations = $this->getOperations();

        if (!$availableOperations || !array_key_exists($name, $availableOperations)) {
            //Проверим, может доступна операция для администраторов
            $availableOperations = $this->getAdminOperations();
            if (!$availableOperations || !array_key_exists($name, $availableOperations)) {
                return;
            }
        }

        $this->$name($user);
    }


    private function orderDelivery($user)
    {
        $this->status = self::STATUS_IN_PROCESS;
        //TODO Вызов метода службы доставки
    }


    private function post($user)
    {
        $this->status = self::STATUS_SEND;
        //TODO Вызов метода службы доставки
    }


    private function complete($user)
    {
        $this->status = self::STATUS_RECEIVE;
        //TODO Вызов метода службы доставки
    }
}