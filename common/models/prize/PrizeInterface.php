<?php

namespace common\models\prize;

interface PrizeInterface
{
    public static function generate($params = null);


    public function getIsCompleted();


    public function getOperations();


    public function getStatusName();


    public function executeOperation($name, $user);


    public function accept();


    public function getPrizeAmount();
}