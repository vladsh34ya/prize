<?php

namespace common\models\prize\fond;

class ThingFond
{
    public static function getThings()
    {
        //По идее нужна таблица, куда можно приходовать конкретные вещи,
        //можно также приходовать их в каком-то количестве

        $things = [
            new Thing(1, 'Лодочка'),
            new Thing(2, 'Машинка'),
            new Thing(3, 'Самолетик'),
            new Thing(4, 'Пароходик'),
            new Thing(5, 'Вертолетик'),
            new Thing(6, 'Подлодочка'),
            new Thing(7, 'Грузовичок'),
            new Thing(8, 'Самокатик'),
            new Thing(9, 'Трактор'),
        ];

        return $things;
    }


    public static function decrease($thingId)
    {
        //Тут уменьшаем призовой фонд, расходуем указанную вещь с места хранения
    }
}