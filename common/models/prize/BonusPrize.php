<?php

namespace common\models\prize;

use common\models\User;

class BonusPrize extends Prize
{
    const STATUS_NEW      = 1;

    const STATUS_TO_BONUS = 2;

    public $type = 'BonusPrize';

    public $name = 'Бонусные баллы';

    public $amount;

    public $transactionId;


    public function getFields()
    {
        return ['name', 'type', 'status', 'amount', 'transactionId'];
    }


    public static function generate($params = null)
    {
        if (!$params || !isset($params['amount'])) {
            return null;
        }

        $maxAmount = $params['amount'];

        $prize         = new static();
        $prize->amount = rand(1, $maxAmount); //TODO 1?
        $prize->status = self::STATUS_NEW;

        return ($prize->amount > 0) ? $prize : null;
    }


    public function accept()
    {
        //Бонусы безлимитны, при получении приза пользователем действий никаких производить не нужно
    }


    public function getPrizeAmount()
    {
        return $this->amount . ' бонусов';
    }


    public function getIsCompleted()
    {
        return ($this->status == self::STATUS_NEW) ? false : true;
    }


    public function getOperations()
    {
        if ($this->status == self::STATUS_NEW) {
            return [
                'sendToBonusAccount' => 'Зачислить на счет лояльности',
            ];
        }

        return null;
    }


    public function getStatusName()
    {
        $statusNames = [
            self::STATUS_NEW      => 'Новый',
            self::STATUS_TO_BONUS => 'Зачислено на счет лояльности',
        ];

        return $statusNames[ $this->status ];
    }


    public function executeOperation($name, $user)
    {
        //Проверим, что операция доступна
        $availableOperations = $this->getOperations();

        if (!$availableOperations || !array_key_exists($name, $availableOperations)) {
            return;
        }

        $this->$name($user);
    }


    /**
     * @param User $user
     */
    private function sendToBonusAccount($user)
    {
        $this->status = self::STATUS_TO_BONUS;

        $this->transactionId = $user->addBonus($this->amount);
    }
}