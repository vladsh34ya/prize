<?php

namespace common\models\prize;

use yii\helpers\Json;

abstract class Prize implements PrizeInterface
{
    const TYPE_MONEY = 'MoneyPrize';

    const TYPE_BONUS = 'BonusPrize';

    const TYPE_THING = 'ThingPrize';

    public $name;

    public $type;

    public $status;


    public function __construct($json = null)
    {
        if (!$json) {
            return;
        }

        $jsonData = Json::decode($json);
        foreach ($this->getFields() as $field) {
            if (isset($jsonData[ $field ])) {
                $this->$field = $jsonData[ $field ];
            }
        }
    }


    public static function getByType($type, $json = null)
    {
        $childClasses = self::getPrizeTypes();

        if (!in_array($type, $childClasses)) {
            return null;
        }

        $className = 'common\models\prize\\' . $type;

        return new $className($json);
    }


    public static function getPrizeTypes()
    {
        return [
            self::TYPE_MONEY,
            self::TYPE_BONUS,
            self::TYPE_THING,
        ];
    }


    public function getAsJson()
    {
        return Json::encode($this);
    }


    public function getFields()
    {
        return ['name', 'type', 'status'];
    }
}