<?php

namespace common\models;

use common\models\prize\PrizeGenerator;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string  $username
 * @property string  $password_hash
 * @property string  $password_reset_token
 * @property string  $email
 * @property string  $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string  $password             write-only password
 * @property integer $account_no           Номер банковского счета
 * @property integer $postal_address       Почтовый адрес
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;

    const STATUS_ACTIVE  = 10;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
        ];
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }


    /**
     * Finds user by username
     *
     * @param string $username
     *
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     *
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne(
            [
                'password_reset_token' => $token,
                'status'               => self::STATUS_ACTIVE,
            ]
        );
    }


    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     *
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire    = Yii::$app->params['user.passwordResetTokenExpire'];

        return $timestamp + $expire >= time();
    }


    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }


    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }


    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }


    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }


    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }


    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }


    #region Логика приложения
    public function getBonuses()
    {
        return BonusMovement::find()->where(['user_id' => $this->id])->sum('amount');
    }


    public function addBonus($amount)
    {
        $move            = new BonusMovement();
        $move->user_id   = $this->id;
        $move->move_type = BonusMovement::MOVE_TYPE_IN;
        $move->amount    = $amount;
        if (!$move->save()) {
            return null;
        }

        return $move->id;
    }


    public function addWithdrawals($amount)
    {
        //TODO Нужно либо приходовать на счет сразу, либо изменить название, сделать AccountMovement просто историев выводов
        //AccountMovement - по идее хранит транзакции по денежному счету клиента, чтобы расчитать текущий остаток
        //Чтобы была возможность дополнить функционал поплнением денежного счета.
        //Сотт-но приз должен сперва оприходовть средства на этот счет, а уже потом списать

        $move            = new AccountMovement();
        $move->user_id   = $this->id;
        $move->move_type = AccountMovement::MOVE_TYPE_IN;
        $move->status    = AccountMovement::STATUS_ACCEPT;
        $move->amount    = $amount;
        if (!$move->save()) {
            return null;
        }

        $move            = new AccountMovement();
        $move->user_id   = $this->id;
        $move->move_type = AccountMovement::MOVE_TYPE_OUT;
        $move->status    = AccountMovement::STATUS_NEW;
        $move->amount    = -$amount;
        if (!$move->save()) {
            return null;
        }

        return $move->id;
    }


    public function getMyPrizes()
    {
        return UserPrize::find()
                        ->where(['user_id' => $this->id])
                        ->andWhere(['>', 'status', UserPrize::STATUS_NEW])
                        ->orderBy(['status' => SORT_ASC, 'created' => SORT_DESC])
                        ->all();
    }


    public function getPrizeForAccept()
    {
        return UserPrize::find()
                        ->where(['user_id' => $this->id, 'status' => UserPrize::STATUS_NEW])
                        ->one();
    }


    public function addNewPrize()
    {
        if ($prize = $this->getPrizeForAccept()) {
            //Уже есть приз
            return $prize;
        }
        $prize = PrizeGenerator::generateNew();
        if (!$prize) {
            return null;
        }

        $userPrize          = new UserPrize();
        $userPrize->user_id = $this->id;
        $userPrize->prize   = $prize;
        if ($userPrize->save()) {
            return $userPrize;
        }

        return null;
    }

    #endregion
}
