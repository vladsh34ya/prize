<?php

namespace common\services;

class BankApi
{
    const LOGIN       = 'qwerty';

    const MERCHANT_ID = '123456';

    const PSW         = 'zxcvb';


    public static function sendTransaction($accountNo, $orderId, $amount)
    {
        $srv = 'https://адрес-банка.рф:9999';
        $url = $srv . '/' .
               self::MERCHANT_ID . '/' .
               'add-new' . '/' .
               $accountNo . '/' .
               $orderId . '/' .
               $amount;

        $headers = [
            "Version: 2",
            "ClientVersion: 1.4.2",
            "Host: https://адрес-банка.рф:9999",
            "Connection: Keep-Alive",
            "Accept-Encoding: gzip",
            "User-Agent: okhttp/3.0.1",
        ];

        $ch       = curl_init();
        $login    = self::LOGIN;
        $password = self::PSW;

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, "$login:$password");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $res = curl_exec($ch);

        if (curl_errno($ch)) {
            return false;
        } else {
            $data = json_decode($res, true);
            if (array_key_exists('document', $data)) {
                return true;
            }
        }

        return false;
    }
}