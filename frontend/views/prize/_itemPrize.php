<?php
/**
 * @var \common\models\UserPrize $userPrize
 */

use yii\helpers\Html;
use common\models\UserPrize;

$operations = $userPrize->prize->getOperations();
?>

<tr>
    <td>
        <?= $userPrize->prize->name ?>
        <br>
        <?= $userPrize->prize->getPrizeAmount() ?>
    </td>
    <td><?= date('d.m.Y H:i', $userPrize->created) ?></td>
    <td><?= $userPrize->getStatusName() ?></td>
    <td>
        <?php if ($operations && count($operations) > 0): ?>
            <?php foreach ($operations as $operationId => $operationName): ?>
                <?= Html::a(
                    $operationName,
                    ['operation', 'id' => $userPrize->id, 'operation' => $operationId],
                    ['class' => 'Operation']
                ) ?>
                <br>
            <?php endforeach; ?>
        <?php endif; ?>
    </td>
</tr>