<?php

/**
 * @var  yii\web\View               $this
 * @var  \common\models\UserPrize   $lastPrize
 * @var  \common\models\UserPrize[] $myPrizes
 */

use yii\helpers\Html;
use frontend\assets\PrizeAsset;

$this->title                   = 'Призы';
$this->params['breadcrumbs'][] = $this->title;

PrizeAsset::register($this);
?>

<div>
    <?= Html::a(
        'Получить новый приз',
        ['get-prize'],
        ['id' => 'prizeGetNew', 'style' => ($lastPrize) ? 'display:none' : '']
    ) ?>
    <div id="newPrize">
        <?php if ($lastPrize): ?>
            <?= $this->render('_newPrize', ['userPrize' => $lastPrize]) ?>
        <?php endif; ?>
    </div>
    <br><br>
    <?php if ($myPrizes && count($myPrizes) > 0): ?>
        <p>Мои призы</p>
        <table>
            <tr>
                <td>Приз</td>
                <td>Получен</td>
                <td>Статус</td>
                <td>...</td>
            </tr>
            <?php foreach ($myPrizes as $userPrize): ?>
                <?= $this->render('_itemPrize', ['userPrize' => $userPrize]) ?>
            <?php endforeach; ?>
        </table>
    <?php endif; ?>
</div>
