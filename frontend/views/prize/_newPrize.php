<?php
/**
 * @var \common\models\UserPrize $userPrize
 */

use yii\helpers\Html;

?>

<p>Вы выиграли приз:</p>
<p><?= $userPrize->prize->name ?>&nbsp;<?= $userPrize->prize->getPrizeAmount() ?></p>
<?= Html::a('Принять', ['accept', 'id' => $userPrize->id], ['id' => 'prizeAccept']) ?>
<br>
<?= Html::a('Отклонить', ['decline', 'id' => $userPrize->id], ['id' => 'prizeDecline']) ?>

