var Prize = {
    init: function () {
        $(document).on('click', '#prizeGetNew', Prize.getNew);

        $(document).on('click', '#prizeAccept', Prize.accept);

        $(document).on('click', '#prizeDecline', Prize.decline);

        $(document).on('click', '.Operation', Prize.operation);
    },

    getNew: function (event) {
        event.preventDefault();
        var url = event.target.href;
        $.getJSON(
            url,
            function (response) {
                if (response.error) {
                    return false;
                }

                if (response.html) {
                    console.log(response.html);
                    $('#newPrize').html(response.html); //TODO
                    $('#prizeGetNew').hide();
                }
            }
        );
    },

    accept: function (event) {
        event.preventDefault();
        var url = event.target.href;
        $.getJSON(
            url,
            function (response) {
                if (response.error) {
                    return false;
                }

                if (response.html) {
                    console.log(response.html);
                    Prize.clearNewPrize();
                    //Вставить в таблицу полученную строку (вверх)
                }
            }
        );
    },

    decline: function (event) {
        event.preventDefault();
        var url = event.target.href;
        $.getJSON(
            url,
            function (response) {
                if (response.error) {
                    return false;
                }

                if (response.ok) {
                    Prize.clearNewPrize();
                }
            }
        );
    },

    operation: function (event) {
        event.preventDefault();
        var url = event.target.href;
        $.getJSON(
            url,
            function (response) {
                if (response.error) {
                    return false;
                }

                if (response.html) {
                    console.log(response.html);
                    //Обновить содержимое строки
                }
            }
        );
    },

    clearNewPrize: function () {
        $('#prizeGetNew').show();
        $('#newPrize').html('');
    },

};

$(document).ready(Prize.init);