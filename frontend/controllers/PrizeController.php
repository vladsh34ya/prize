<?php

namespace frontend\controllers;

use common\models\User;
use common\models\UserPrize;
use Yii;
use yii\web\Response;
use yii\web\Controller;
use yii\filters\AccessControl;


/**
 * Prize controller
 */
class PrizeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ];
    }


    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var User $user */
        $user = Yii::$app->user->identity;

        $lastPrize = $user->getPrizeForAccept();
        $MyPrizes  = $user->getMyPrizes();

        return $this->render('index', ['lastPrize' => $lastPrize, 'myPrizes' => $MyPrizes]);
    }


    public function actionGetPrize()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $user */
        $user = Yii::$app->user->identity;

        $userPrize = $user->addNewPrize();
        if ($userPrize) {
            return [
                'result' => 'ok',
                'html'   => $this->renderAjax('_newPrize', ['userPrize' => $userPrize,]),
            ];
        }

        return [
            'error' => 'Не удалось получить приз',
        ];
    }


    public function actionAccept($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $user */
        $user = Yii::$app->user->identity;

        $userPrize = UserPrize::findOne(['id' => $id, 'user_id' => $user->id]);
        if ($userPrize) {
            $userPrize->accept();

            return [
                'result' => 'ok',
                'html'   => $this->renderAjax('_itemPrize', ['userPrize' => $userPrize,]),
            ];
        }

        return [
            'error' => 'Не удалось выполнить операцию',
        ];
    }


    public function actionDecline($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        /** @var User $user */
        $user = Yii::$app->user->identity;

        $userPrize = UserPrize::findOne(['id' => $id, 'user_id' => $user->id]);
        if ($userPrize) {
            $userPrize->decline();

            return [
                'ok' => true,
            ];
        }

        return [
            'error' => 'Не удалось выполнить операцию',
        ];
    }


    public function actionOperation($id, $operation)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        if (!$id || !$operation) {
            return; //TODO
        }

        /** @var User $user */
        $user = Yii::$app->user->identity;

        $userPrize = UserPrize::findOne(['id' => $id, 'user_id' => $user->id]);
        if ($userPrize) {
            $userPrize->executeOperation($operation);

            return [
                'result' => 'ok',
                'html'   => $this->renderAjax('_itemPrize', ['userPrize' => $userPrize,]),
            ];
        }

        return [
            'error' => 'Не удалось выполнить операцию',
        ];
    }

}
