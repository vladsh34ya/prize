<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class PrizeAsset extends AssetBundle
{
    public $basePath = '@webroot';

    public $baseUrl  = '@web';

    public $css      = [
        'css/site.css',
        'css/prize.css',
    ];

    public $js       = [
        'js/prize.js',
    ];

    public $depends  = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
