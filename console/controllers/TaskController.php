<?php

namespace console\controllers;

use common\models\AccountMovement;
use common\models\BankHelper;
use common\models\prize\Prize;
use common\models\UserPrize;
use yii\console\Controller;

class TaskController extends Controller
{
    public function actionSendMoneyToUser($count = 10)
    {
        $transactions = AccountMovement::find()
                                       ->where(['status' => AccountMovement::STATUS_NEW])
                                       ->orderBy(['created' => SORT_DESC])
                                       ->limit($count)
                                       ->all();

        foreach ($transactions as $transaction) {
            /** @var AccountMovement $transaction */
            $res                 = BankHelper::sendToBank($transaction);
            $transaction->status = ($res) ? AccountMovement::STATUS_ACCEPT : AccountMovement::STATUS_DECLINE;
            $transaction->save();
        }
    }


    public function actionSendThings($count = 10)
    {
        $things = UserPrize::find()
                           ->where(['type' => Prize::TYPE_THING, 'status' => UserPrize::STATUS_IN_PROCESS])
                           ->orderBy(['created' => SORT_DESC])
                           ->limit($count)
                           ->all();

        foreach ($things as $thing) {
            /** @var UserPrize $thing */
            $thing->executeOperation('post'); //TODO - по идее это нужно делать в подсистеме доставки
        }
    }

}