<?php

use yii\db\Migration;

/**
 * Class m180731_120921_addBaseModel
 */
class m180731_120921_addBaseModel extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        //Доп поля для User
        $this->addColumn('user', 'account_no', $this->string()); //Номер банковского счета
        $this->addColumn('user', 'postal_address', $this->string()); //Почтовый адрес


        //Движения по банковскому счету пользователя
        $this->createTable(
            '{{%account_movements}}',
            [
                'id'        => $this->primaryKey(),
                'user_id'   => $this->integer()->comment('Id пользователя'),
                'created'   => $this->integer()->comment('Создано'),
                'move_type' => $this->integer()->comment('Тип движения'),
                'amount'    => $this->float()->comment('Сумма'),
                'status'    => $this->integer()->comment('Статус'),
                'updated'   => $this->integer()->comment('Обновлено'),
            ],
            $tableOptions
        );


        //Движения по счету лояльности пользователя
        $this->createTable(
            '{{%bonus_movements}}',
            [
                'id'        => $this->primaryKey(),
                'user_id'   => $this->integer()->comment('Id пользователя'),
                'created'   => $this->integer()->comment('Создано'),
                'move_type' => $this->integer()->comment('Обновлено'),
                'amount'    => $this->float()->comment('Сумма'),
            ],
            $tableOptions
        );


        //Хранилище призов пользователя
        $this->createTable(
            '{{%user_prizes}}',
            [
                'id'            => $this->primaryKey(),
                'user_id'       => $this->integer()->comment('Id пользователя'),
                'created'       => $this->integer()->comment('Создано'),
                'updated'       => $this->integer()->comment('Обновлено'),
                'type'          => $this->string()->comment('Тип приза'),
                //изменил миграцию, но это тестовый пример, надеюсь не страшно
                'prize_as_json' => $this->text()->comment('Данные о призе в формате JSON'),
                'status'        => $this->integer()->comment('Статус'),
            ],
            $tableOptions
        );
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180731_120921_addBaseModel cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180731_120921_addBaseModel cannot be reverted.\n";

        return false;
    }
    */
}
